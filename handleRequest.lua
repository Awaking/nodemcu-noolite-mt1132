local mt1132Pin=3;
local mt1132Rate=9600;
---------
local function emptyBuffer()
    return {85,80,0,0,0,0,0,0,0,0,0,170};
end
---------
local function sendBuffer(buffer)
    buffer[11]=checksum(buffer);
    for i = 1, #buffer, 1 do
       print(buffer[i]);
        softuart.write(mt1132Pin
                       ,mt1132Rate
                       ,buffer[i]);
    end
end
---------
function checksum(buffer)
    local chkSum=0;
    for i = 1, #buffer-1, 1 do
        chkSum=chkSum+buffer[i];
    end
    return  chkSum - math.floor(chkSum/256)*256
end
---------
local function getNumberForKey(table, key, minVal, maxVal)
    local value=table[key];
    if(not nilOrEmpty(value)) then
        value = tonumber(value);
        if(value ~= nil and value >=minVal and value <= maxVal) then
            return value;
        end
    end
    print ("ERROR! Key "..key.." not defined!");
    return nil;
end
---------
local function execCommonCmd(table, code)
    local channel=getNumberForKey(table, "channel", 0, 31);
    if(channel ~= nil) then
        local buf=emptyBuffer();
        buf[3]=code;
        buf[6]=channel;
        sendBuffer(buf);
    end
end
---------
local function setBrightness(table)
    local channel=getNumberForKey(table, "channel", 0, 31);
    local brightness=getNumberForKey(table
                                     ,"brightness", 0, 255);
    if(channel ~= nil and brightness ~= nil) then
        local buf=emptyBuffer();
        buf[3]=6;
        buf[4]=1;
        buf[6]=channel;
        buf[7]=brightness;
        sendBuffer(buf);
    end
end
---------
local actionTable={
    ["turn_on"] = {execCommonCmd, 2},
    ["turn_off"] = {execCommonCmd,0},
    ["set_brightness"] = {setBrightness,6},
    ["switch_state"] = {execCommonCmd,4},
    ["attach"] = {execCommonCmd,15},
    ["detach"] = {execCommonCmd,9},
    ["rec_script"] = {execCommonCmd,8},
    ["run_script"] = {execCommonCmd,7},
    ["smooth_up"] = {execCommonCmd,3},
    ["smooth_down"] = {execCommonCmd,1},
    ["smooth_switch"] = {execCommonCmd,5},
    ["smooth_stop"] = {execCommonCmd,10},
    ["smooth_color_change"] = {execCommonCmd,16},
    ["switch_color"] = {execCommonCmd,17},
    ["switch_color_speed"] = {execCommonCmd,19},
}
---------
for k, v in pairs( bodyTable ) do
   print("key="..k .. "; val=" .. v ..";");
end

actionName=string.lower(bodyTable.action);
print("act="..actionName);
if(not nilOrEmpty(actionName) 
   and actionTable[actionName]~=nil) then
     local cmdTable=actionTable[actionName];
     local cmdCode=cmdTable[2];
     local cmdFunc=cmdTable[1];
     cmdFunc(bodyTable, cmdCode);
end
bodyTable=nil;
