sv=net.createServer(net.TCP, 30)
bodyTable = nil
local totalFileLength=0
local lastFileIndex=0;
local requestFileMap={
    ["/"] =           {["file"] = "index.html", ["content-type"] = "text/html"},
    ["/index.html"] = {["file"] = "index.html", ["content-type"] = "text/html"},
    ["/style.css"] =  {["file"] = "style.css",  ["content-type"] = "text/css"},
    ["/test2.html"] = {["file"] = "test2.html", ["content-type"] = "text/html"},
};

local function sendfile(conn, name)
    if (lastFileIndex<totalFileLength and file.open(name, "r")) then
        file.seek("set",lastFileIndex);
        local line=file.read(1024);
        conn:send(line);
        lastFileIndex=lastFileIndex+string.len(line);
        file.close();
        return false;
    end
    return true;
end

local function getFileLength(name)
    local length=0;
    if file.open(name, "r") then
        length=file.seek("end") or 0;
        file.close()
    end;
    return length;
end


local function connect (conn, data)
   totalFileLength=0
   lastFileIndex=0;
   bodyTable = nil;
   print("----[CONNECTED]----");
   conn:on("sent",function(conn)
        print(string.format("----[SENT %d/%d]----", lastFileIndex, totalFileLength));
        if(lastFileIndex>=totalFileLength) then
            conn:close();
            print("----[DISCONNECTED]----");
            totalFileLength=0;
            lastFileIndex=0;
            collectgarbage();
        else
            sendfile(conn, fileName);
        end
    end)

   conn:on ("receive",
      function (cn, req_data)
         print("----[RECEIVED]----");
         print(req_data);
         print("------------------");
         local query_data = get_http_req (req_data);         
         local bodyRaw=nil;
         
         if(nilOrEmpty(query_data["METHOD"])) then
            bodyRaw=req_data;
         else 
            if(trim(query_data["METHOD"])=="post" and
                 not nilOrEmpty(query_data["Content-Type"])) then
                    bodyRaw=trim(query_data["BODY"]);
            end;
         end;
         
         if(bodyRaw~=nil and bodyRaw~="") then
            --Try to parse as JSON
            bodyTable = cjson.decode(bodyRaw);
            dofile("handleRequest.lc");
            cn:send ("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 0\r\nConnection: close\r\n");
         else
            --Handle html
            local path=query_data["PATH"];
            if(requestFileMap[path]) then 
                if(query_data["VARS"]) then
                   bodyTable = query_data["VARS"];
                   dofile("handleRequest.lc");

                end;
                    fileName=requestFileMap[path]["file"];
                    local contentType=requestFileMap[path]["content-type"];
                    totalFileLength=getFileLength(fileName);
                    local headers = string.format("HTTP/1.1 200 OK\r\nContent-Type: %s\r\nConnection: close\r\nContent-Length: %s\r\n\r\n",contentType, tostring(totalFileLength));
                    cn:send(headers);
                    sendfile(cn,fileName);
                 
            else                
                cn:send("HTTP/1.1 404 Not Found\r\nConnection: close\r\n\r\n");
                print("----[SENT '404 not found']----");
            end;
         end;
         query_data = nil;
         bodyRaw = nil;
      end)
end



-- Build and return a table of the http request data
function get_http_req (instr)
print("start get_http_req");
   local t = {};
   local first = nil;
   local last = nil;
   local key, v, strt_ndx, end_ndx;
    instr=instr.."\n";
   for str in string.gmatch (instr, "[^\n]-\n") do
   --print("str="..str..";")
      -- First line in the method and path
      if (first==nil) then
         --print("step-1");
         first = 1
         local _, _, method, path, vars = string.find(str, "([A-Z]+) (.+)?(.+) HTTP");
         if(method == nil) then
            _, _, method, path = string.find(str, "([A-Z]+) (.+) HTTP");
         end
         if(method) then t["METHOD"] = string.lower(method);end;
         if(path) then t["PATH"] = string.lower(path); end;
         if(vars) then         
            
            for k, v in string.gmatch(vars, "([%w_]+)=([%w_]+)&*") do
                if(t["VARS"]==nil) then t["VARS"] = {}; print ("vars init");end;
                print (k.."="..v);
                t["VARS"][k]=string.lower(v);
            end;
         end;
      else 
          if (t["BODY"]==nil and trim(str)=="") then
             last=1 
             --print("step-2");
             t["BODY"]="";
          else 
              if (last==1) then
                 t["BODY"] = t["BODY"] .. trim(str)
                 --print("step-3");
              else
                 -- Process and reamaining ":" fields
                 strt_ndx, end_ndx = string.find (str, "([^:]+)")
                 --print("step-4");
                 if (end_ndx ~= nil) then
                    v = trim (string.sub (str, end_ndx + 2))
                    key = trim (string.sub (str, strt_ndx, end_ndx))
                    t[key] = v
                    --print("step-5");
                 end
              end
          end
      end
   end
   return t
end



sv:listen(80,connect);
