---------
function nilOrEmpty(string)
    return string == nil or string == "";
end
---------
-- String trim left and right
function trim (s)
  return (s:gsub ("^%s*(.-)%s*$", "%1"))
end
